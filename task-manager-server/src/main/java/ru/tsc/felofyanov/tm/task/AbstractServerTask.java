package ru.tsc.felofyanov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull Server server) {
        this.server = server;
    }
}
