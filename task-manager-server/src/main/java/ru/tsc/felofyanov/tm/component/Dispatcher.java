package ru.tsc.felofyanov.tm.component;

import ru.tsc.felofyanov.tm.api.endpoint.Operation;
import ru.tsc.felofyanov.tm.dto.request.AbstractRequest;
import ru.tsc.felofyanov.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            Class<RQ> rqClass, Operation<RQ, RS> operation
    ) {
        map.put(rqClass, operation);
    }

    public Object call(AbstractRequest request) {
        final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }
}
