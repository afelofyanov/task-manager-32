package ru.tsc.felofyanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.felofyanov.tm.api.service.IServiceLocator;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;
import ru.tsc.felofyanov.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public DataLoadBackupResponse loadDataBackup(@NotNull final DataLoadBackupRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataLoadBackupResponse();
    }

    @NotNull
    @Override
    public DataLoadBase64Response loadDataBase64(@NotNull final DataLoadBase64Request request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataLoadBase64Response();
    }

    @NotNull
    @Override
    public DataLoadBinaryResponse loadDataBinary(@NotNull final DataLoadBinaryRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataLoadBinaryResponse();
    }

    @NotNull
    @Override
    public DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(@NotNull final DataLoadJsonFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXml();
        return new DataLoadJsonFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataLoadJsonJaxbResponse loadDataJsonJaxb(@NotNull final DataLoadJsonJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxB();
        return new DataLoadJsonJaxbResponse();
    }

    @NotNull
    @Override
    public DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(@NotNull final DataLoadXmlFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFasterXml();
        return new DataLoadXmlFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataLoadXmlJaxbResponse loadDataXmlJaxb(@NotNull final DataLoadXmlJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlJaxB();
        return new DataLoadXmlJaxbResponse();
    }

    @NotNull
    @Override
    public DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(@NotNull final DataLoadYamlFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFasterXml();
        return new DataLoadYamlFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataSaveBackupResponse saveDataBackup(@NotNull final DataSaveBackupRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataSaveBackupResponse();
    }

    @NotNull
    @Override
    public DataSaveBase64Response saveDataBase64(@NotNull final DataSaveBase64Request request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataSaveBase64Response();
    }

    @NotNull
    @Override
    public DataSaveBinaryResponse saveDataBinary(@NotNull final DataSaveBinaryRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataSaveBinaryResponse();
    }

    @NotNull
    @Override
    public DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(@NotNull final DataSaveJsonFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXml();
        return new DataSaveJsonFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataSaveJsonJaxbResponse saveDataJsonJaxb(@NotNull final DataSaveJsonJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxB();
        return new DataSaveJsonJaxbResponse();
    }

    @NotNull
    @Override
    public DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(@NotNull final DataSaveXmlFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFasterXml();
        return new DataSaveXmlFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataSaveXmlJaxbResponse saveDataXmlJaxb(@NotNull final DataSaveXmlJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxB();
        return new DataSaveXmlJaxbResponse();
    }

    @NotNull
    @Override
    public DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(@NotNull final DataSaveYamlFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFasterXml();
        return new DataSaveYamlFasterXmlResponse();
    }
}
