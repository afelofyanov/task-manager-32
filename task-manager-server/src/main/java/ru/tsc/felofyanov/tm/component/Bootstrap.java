package ru.tsc.felofyanov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.endpoint.*;
import ru.tsc.felofyanov.tm.api.repository.IDomainRepository;
import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.api.repository.IUserRepository;
import ru.tsc.felofyanov.tm.api.service.*;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.endpoint.*;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.model.User;
import ru.tsc.felofyanov.tm.repository.DomainRepository;
import ru.tsc.felofyanov.tm.repository.ProjectRepository;
import ru.tsc.felofyanov.tm.repository.TaskRepository;
import ru.tsc.felofyanov.tm.repository.UserRepository;
import ru.tsc.felofyanov.tm.service.*;
import ru.tsc.felofyanov.tm.util.DateUtil;
import ru.tsc.felofyanov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainRepository domainRepository = new DomainRepository(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(domainRepository);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataLoadBackupRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataLoadBase64Request.class, domainEndpoint::loadDataBase64);
        server.registry(DataLoadBinaryRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataLoadJsonFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataLoadJsonJaxbRequest.class, domainEndpoint::loadDataJsonJaxb);
        server.registry(DataLoadXmlFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataLoadXmlJaxbRequest.class, domainEndpoint::loadDataXmlJaxb);
        server.registry(DataLoadYamlFasterXmlRequest.class, domainEndpoint::loadDataYamlFasterXml);

        server.registry(DataSaveBackupRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataSaveBase64Request.class, domainEndpoint::saveDataBase64);
        server.registry(DataSaveBinaryRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataSaveJsonFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataSaveJsonJaxbRequest.class, domainEndpoint::saveDataJsonJaxb);
        server.registry(DataSaveXmlFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataSaveXmlJaxbRequest.class, domainEndpoint::saveDataXmlJaxb);
        server.registry(DataSaveYamlFasterXmlRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectGetByIdRequest.class, projectEndpoint::getProjectById);
        server.registry(ProjectGetByIndexRequest.class, projectEndpoint::getProjectByIndex);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskGetByIdRequest.class, taskEndpoint::getTaskById);
        server.registry(TaskGetByIndexRequest.class, taskEndpoint::getTaskByIndex);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);

        server.registry(ProjectRemoveByIdRequest.class, projectTaskEndpoint::removeProjectById);
        server.registry(ProjectBindTaskByIdRequest.class, projectTaskEndpoint::bindTaskToProjectId);
        server.registry(ProjectUnbindTaskByIdRequest.class, projectTaskEndpoint::unbindTaskFromProjectId);

        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserProfileRequest.class, userEndpoint::viewProfileUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateProfileUser);
    }

    public void run() {
        initPID();
        initData();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SHUTTING DOWN**");
        backup.stop();
        server.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initData() {
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        @NotNull final User test = userService.create("test", "test", Role.USUAL);

        taskService.add(test.getId(), new Task("DEMO TASK", Status.IN_PROGRESS, DateUtil.toDate("06.05.2019")));
        taskService.add(test.getId(), new Task("TEST TASK", Status.NOT_STARTED, DateUtil.toDate("03.01.2017")));
        taskService.add(test.getId(), new Task("MEGA TASK", Status.COMPLETED, DateUtil.toDate("08.07.2021")));
        taskService.add(test.getId(), new Task("BEST TASK", Status.IN_PROGRESS, DateUtil.toDate("05.05.2018")));

        projectService.add(test.getId(), new Project("DEMO PROJECT", Status.NOT_STARTED, DateUtil.toDate("05.06.2019")));
        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS, DateUtil.toDate("03.01.2017")));
        projectService.add(test.getId(), new Project("MEGA PROJECT", Status.COMPLETED, DateUtil.toDate("08.07.2021")));
        projectService.add(test.getId(), new Project("BEST PROJECT", Status.IN_PROGRESS, DateUtil.toDate("05.05.2018")));
    }
}
