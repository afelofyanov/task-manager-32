package ru.tsc.felofyanov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.enumerated.Role;

public class DisconnectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "disconnect";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Disconnect from server";
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[0];
    }

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().disconnect();
    }
}
