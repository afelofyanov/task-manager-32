package ru.tsc.felofyanov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.felofyanov.tm.dto.request.ProjectBindTaskByIdRequest;
import ru.tsc.felofyanov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.felofyanov.tm.dto.request.ProjectUnbindTaskByIdRequest;
import ru.tsc.felofyanov.tm.dto.response.ProjectBindTaskByIdResponse;
import ru.tsc.felofyanov.tm.dto.response.ProjectRemoveByIdResponse;
import ru.tsc.felofyanov.tm.dto.response.ProjectUnbindTaskByIdResponse;

@NoArgsConstructor
public final class ProjectTaskEndpointClient extends AbstractEndpointClient implements IProjectTaskEndpoint {

    public ProjectTaskEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectBindTaskByIdResponse bindTaskToProjectId(@NotNull ProjectBindTaskByIdRequest request) {
        return call(request, ProjectBindTaskByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUnbindTaskByIdResponse unbindTaskFromProjectId(@NotNull ProjectUnbindTaskByIdRequest request) {
        return call(request, ProjectUnbindTaskByIdResponse.class);
    }
}
