package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ProjectTaskEndpointClient getProjectTaskEndpoint();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    UserEndpointClient getUserEndpoint();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    DomainEndpointClient getDomainEndpoint();
}
