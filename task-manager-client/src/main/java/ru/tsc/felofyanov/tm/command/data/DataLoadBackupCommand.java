package ru.tsc.felofyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.DataLoadBackupRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;

public final class DataLoadBackupCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "load-backup";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load backup from file";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getDomainEndpoint().loadDataBackup(new DataLoadBackupRequest());
    }
}
