package ru.tsc.felofyanov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.service.IServiceLocator;
import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "connect";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Connect to server";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        serviceLocator.getAuthEndpoint().connect();
        final Socket socket = serviceLocator.getAuthEndpoint().getSocket();

        serviceLocator.getProjectEndpoint().setSocket(socket);
        serviceLocator.getTaskEndpoint().setSocket(socket);
        serviceLocator.getProjectTaskEndpoint().setSocket(socket);
        serviceLocator.getUserEndpoint().setSocket(socket);
    }
}
