package ru.tsc.felofyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.felofyanov.tm.dto.response.ProjectRemoveByIdResponse;
import ru.tsc.felofyanov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();

        @NotNull final ProjectRemoveByIdRequest projectRemoveByIdRequest = new ProjectRemoveByIdRequest(id);
        @NotNull final ProjectRemoveByIdResponse projectRemoveByIdResponse
                = getServiceLocator().getProjectEndpoint().removeProjectById(projectRemoveByIdRequest);
        @Nullable final Project project = projectRemoveByIdResponse.getProject();
        if (project == null) throw new ProjectNotFoundException();
    }
}
