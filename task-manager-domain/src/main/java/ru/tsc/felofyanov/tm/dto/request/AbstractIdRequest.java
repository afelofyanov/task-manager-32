package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public AbstractIdRequest(@Nullable String id) {
        this.id = id;
    }
}
