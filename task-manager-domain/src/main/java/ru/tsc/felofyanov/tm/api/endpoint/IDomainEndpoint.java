package ru.tsc.felofyanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;

public interface IDomainEndpoint {

    @NotNull
    DataLoadBackupResponse loadDataBackup(@NotNull DataLoadBackupRequest request);

    @NotNull
    DataLoadBase64Response loadDataBase64(@NotNull DataLoadBase64Request request);

    @NotNull
    DataLoadBinaryResponse loadDataBinary(@NotNull DataLoadBinaryRequest request);

    @NotNull
    DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(@NotNull DataLoadJsonFasterXmlRequest request);

    @NotNull
    DataLoadJsonJaxbResponse loadDataJsonJaxb(@NotNull DataLoadJsonJaxbRequest request);

    @NotNull
    DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(@NotNull DataLoadXmlFasterXmlRequest request);

    @NotNull
    DataLoadXmlJaxbResponse loadDataXmlJaxb(@NotNull DataLoadXmlJaxbRequest request);

    @NotNull
    DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(@NotNull DataLoadYamlFasterXmlRequest request);

    @NotNull
    DataSaveBackupResponse saveDataBackup(@NotNull DataSaveBackupRequest request);

    @NotNull
    DataSaveBase64Response saveDataBase64(@NotNull DataSaveBase64Request request);

    @NotNull
    DataSaveBinaryResponse saveDataBinary(@NotNull DataSaveBinaryRequest request);

    @NotNull
    DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(@NotNull DataSaveJsonFasterXmlRequest request);

    @NotNull
    DataSaveJsonJaxbResponse saveDataJsonJaxb(@NotNull DataSaveJsonJaxbRequest request);

    @NotNull
    DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(@NotNull DataSaveXmlFasterXmlRequest request);

    @NotNull
    DataSaveXmlJaxbResponse saveDataXmlJaxb(@NotNull DataSaveXmlJaxbRequest request);

    @NotNull
    DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(@NotNull DataSaveYamlFasterXmlRequest request);
}
