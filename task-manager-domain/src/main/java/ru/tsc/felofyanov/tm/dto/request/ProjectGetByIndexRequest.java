package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectGetByIndexRequest extends AbstractIndexRequest {

    public ProjectGetByIndexRequest(@Nullable Integer index) {
        super(index);
    }
}
