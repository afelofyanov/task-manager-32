package ru.tsc.felofyanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserProfileResponse viewProfileUser(@NotNull UserProfileRequest request);

    @NotNull
    UserUpdateProfileResponse updateProfileUser(@NotNull UserUpdateProfileRequest request);
}
