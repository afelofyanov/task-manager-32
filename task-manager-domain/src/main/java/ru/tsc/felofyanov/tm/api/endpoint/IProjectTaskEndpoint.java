package ru.tsc.felofyanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.dto.request.ProjectBindTaskByIdRequest;
import ru.tsc.felofyanov.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.felofyanov.tm.dto.request.ProjectUnbindTaskByIdRequest;
import ru.tsc.felofyanov.tm.dto.response.ProjectBindTaskByIdResponse;
import ru.tsc.felofyanov.tm.dto.response.ProjectRemoveByIdResponse;
import ru.tsc.felofyanov.tm.dto.response.ProjectUnbindTaskByIdResponse;

public interface IProjectTaskEndpoint {

    @NotNull
    ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectBindTaskByIdResponse bindTaskToProjectId(@NotNull ProjectBindTaskByIdRequest request);

    @NotNull
    ProjectUnbindTaskByIdResponse unbindTaskFromProjectId(@NotNull ProjectUnbindTaskByIdRequest request);
}
