package ru.tsc.felofyanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.dto.request.ServerAboutRequest;
import ru.tsc.felofyanov.tm.dto.request.ServerVersionRequest;
import ru.tsc.felofyanov.tm.dto.response.ServerAboutResponse;
import ru.tsc.felofyanov.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);
}
